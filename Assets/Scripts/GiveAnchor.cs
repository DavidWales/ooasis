﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;

public class GiveAnchor : MonoBehaviour
{
    Anchor myAnchor;
    Pose pose;

    private void Start()
    {
        pose = new Pose(this.transform.position, this.transform.rotation);
        Session.CreateAnchor(this.pose);
    }
}
