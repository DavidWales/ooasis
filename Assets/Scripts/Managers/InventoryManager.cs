﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Manages all interaction with the inventory.
/// </summary>
public class InventoryManager : MonoBehaviour
{

    #region Properties

    public List<ItemDefinition> Items;
    public List<InventoryDefinition> Inventory;

    private ItemType activeItem = 0;

    #endregion

    #region Item Management

    /// <summary>
    /// Set the currently active item. To do so, it deactivates the currently active item,
    /// and then activates the new item asked to be equipped.
    /// </summary>
    /// <param name="newItem">The new item to be equipped.</param>
    public static void SetItem(ItemType newItem)
    {
        _inst.Items.FirstOrDefault(i => i.type == _inst.activeItem).obj.SetActive(false);
        _inst.Items.FirstOrDefault(i => i.type == newItem).obj.SetActive(true);
        _inst.activeItem = newItem;

    }

    /// <summary>
    /// Retrieves the item count for the specified item, as determined by the current inventory.
    /// </summary>
    /// <param name="itemType">The item to determine the count for.</param>
    /// <returns>The count stored in the inventory for the specified item.</returns>
    public static int GetItemCount(ItemType itemType)
    {
        int result = 0;
        if (_inst.Inventory.Any(i => i.type == itemType))
        {
            return _inst.Inventory.FirstOrDefault(i => i.type == itemType).count;
        }
        return result;
    }

    /// <summary>
    /// Adds count to specific inventory item
    /// </summary>
    /// <param name="itemType">Item type to add to count</param>
    /// <param name="value">Count to add</param>
    public static void AddItemCount(ItemType itemType, int value)
    {
        if(_inst.Inventory.Any(i => i.type == itemType) == false)
        {
            _inst.Inventory.Add(new InventoryDefinition
            {
                type = itemType,
                count = 0
            });
        }

        _inst.Inventory.FirstOrDefault(i => i.type == itemType).count += value;
    }

    #endregion

    #region Singleton Stuff

    private static InventoryManager _inst = null;

    protected virtual void Awake()
    {
        if(_inst != null)
        {
            DestroyImmediate(this.gameObject);
        }
        _inst = this;
        SetItem(activeItem);
    }

    #endregion

}