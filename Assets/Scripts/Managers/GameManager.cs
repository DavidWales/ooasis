﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the general game settings and game loop
/// </summary>
public class GameManager : MonoBehaviour
{
    public RawImageFader logo;

    protected virtual void Awake()
    {
        //Lock the screen to portait mode, but allow them to flip it upside down (why not)
        Screen.orientation = ScreenOrientation.Portrait;
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
    }

    protected virtual void Start()
    {
        //Fade out the logo upon game start
        logo.FadeOut(() => { });
    }
}