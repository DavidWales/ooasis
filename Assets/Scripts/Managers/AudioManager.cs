﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager audioManager;

    public GameObject mainTheme, uiSelect, WateringNoise, groundAppear, sproutAppear, wateringComplete;

    private void Start()
    {
        audioManager = this;
    }

    public void PlayTheme()
    {
        this.mainTheme.GetComponent<AudioSource>().Play();
    }
    public void PlayUISelect()
    {
        this.uiSelect.GetComponent<AudioSource>().Play();
    }
    public void PlayWateringNoise()
    {
        if (this.WateringNoise.GetComponent<AudioSource>().isPlaying == false)
        {
            this.WateringNoise.GetComponent<AudioSource>().Play();
        }
    }
    public void StopWateringNoise()
    {
        this.WateringNoise.GetComponent<AudioSource>().Stop();
    }
    public void PlayGroundAppear()
    {
        this.groundAppear.GetComponent<AudioSource>().Play();
    }
    public void PlaySproutAppear()
    {
        this.sproutAppear.GetComponent<AudioSource>().Play();
    }
    public void PlayWateringComplete()
    {
        this.wateringComplete.GetComponent<AudioSource>().Play();
    }
}
