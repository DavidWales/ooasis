﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

enum LightState
{
    DAY,
    NIGHT,
    NAMELESS
}
namespace GoogleARCore
{
    [ExecuteInEditMode]
    public class LightManager : MonoBehaviour
    {
        [SerializeField]
        LightState lightState = LightState.NAMELESS;

        public GameObject[] billboardSprites;
        public GameObject[] nightObjects;
        public GameObject[] clouds;

        //for clouds
        public Material oldMaterial, glowMaterial;
        public Animator anim;

        public float lightThreshold;

        public void Update()
        {
            this.DetectLight();
            this.BillboardSprites();
        }

        void DetectLight()
        {
            if (Frame.LightEstimate.State != LightEstimateState.Valid)
            {
                return;
            }

            if (Frame.LightEstimate.PixelIntensity < lightThreshold)
            {
                if (this.lightState != LightState.NIGHT)
                {
                    this.SetNight();
                }
            }
            else
            {
                if (this.lightState != LightState.DAY)
                {
                    this.SetDay();
                }
            }
        }
        void SetDay()
        {
            if(this.lightState == LightState.NIGHT)
            {
                //perform Night to day animation
                //turn on night time things, turn off day time things
                this.anim.Play("MoonToSun");
                this.TurnOffNightObjects();
            }

            this.lightState = LightState.DAY;
        }

        void SetNight()
        {
            if(this.lightState == LightState.DAY)
            {
                //activate day to night animations
                //turn off night things and turn on day things
                this.anim.Play("SunToMoon");
                this.TurnOnNightObjects();
            }

            this.lightState = LightState.NIGHT;
        }

        void BillboardSprites()
        {
            for(int i = 0; i < this.billboardSprites.Length; i++)
            {
                this.billboardSprites[i].transform.forward = Camera.main.transform.forward;
            }
        }

        void TurnOnNightObjects()
        {
            for (int i = 0; i < this.nightObjects.Length; i++)
            {
                this.nightObjects[i].SetActive(true);
            }

            for(int i = 0; i < this.clouds.Length; i++)
            {
                FireflyController fireFly = this.clouds[i].GetComponent<FireflyController>();
                if (fireFly != null)
                {
                    fireFly.ActivateFireFly();
                }
            }
        }

        void TurnOffNightObjects()
        {
            for (int i = 0; i < this.nightObjects.Length; i++)
            {
                FireflyController fireFly = this.nightObjects[i].GetComponent<FireflyController>();
                if(fireFly != null)
                {
                    fireFly.DeactivateFireFly();
                }
            }

            for (int i = 0; i < this.clouds.Length; i++)
            {
                this.clouds[i].GetComponent<SpriteRenderer>().material = this.oldMaterial;
            }
        }
    }
}

