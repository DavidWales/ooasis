﻿using GoogleARCore;
using GoogleARCore.Examples.Common;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the ARCore planes. Serves as a shared resource for all other
/// components to retrieve information about the detected planes.
/// </summary>
public class PlaneManager : MonoBehaviour
{

    public GameObject visualizerPrefab;
    public bool visualize = false;

    private List<DetectedPlane> allPlanes = new List<DetectedPlane>();

    private void Update()
    {
        Session.GetTrackables(allPlanes, TrackableQueryFilter.All);
        if (visualize == true) Visualize();
    }

    /// <summary>
    /// Retrieve all of the currently tracked planes. This is updated every frame.
    /// </summary>
    /// <returns>All of the currently tracked planes detected by ARCore</returns>
    public static List<DetectedPlane> GetAllPlanes()
    {
        return _inst.allPlanes;
    }

    /// <summary>
    /// Use the generic Google ARCore visualization prefab to visualize newly
    /// detected planes.
    /// </summary>
    protected virtual void Visualize()
    {
        if (visualizerPrefab != null)
        {
            List<DetectedPlane> newPlanes = new List<DetectedPlane>();
            Session.GetTrackables(newPlanes, TrackableQueryFilter.New);
            foreach (DetectedPlane newPlane in newPlanes)
            {
                GameObject newVisual = Instantiate(visualizerPrefab, Vector3.zero, Quaternion.identity, transform);
                newVisual.GetComponent<DetectedPlaneVisualizer>().Initialize(newPlane);
            }
        }
    }

    #region Singleton Stuff

    private static PlaneManager _inst = null;

    protected virtual void Awake()
    {
        if(_inst != null)
        {
            DestroyImmediate(this.gameObject);
        }
        _inst = this;
    }

    #endregion

}