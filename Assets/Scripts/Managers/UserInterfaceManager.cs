﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages all of the user interface interactions.
/// </summary>
public class UserInterfaceManager : MonoBehaviour
{
    #region Properties

    public GameObject buttonContainer;

    #endregion

    #region Button Click Events

    /// <summary>
    /// Toggles the visibility of the item buttons.
    /// </summary>
    public void OnInventoryButtonClick()
    {
        buttonContainer.SetActive(!buttonContainer.activeInHierarchy);
    }

    /// <summary>
    /// Swap to a different item.
    /// </summary>
    /// <param name="itemType">Item Type to switch to.</param>
    public static void OnItemButtonClick(ItemType itemType)
    {
        switch (itemType)
        {
            case ItemType.WATERING_CAN:
                AudioManager.audioManager.PlayUISelect();
                InventoryManager.SetItem(itemType);
                break;
            default:
                _inst.TryEquipItem(itemType);
                break;
        }
    }

    /// <summary>
    /// Only allow an item swap if the item has charges
    /// </summary>
    /// <param name="itemType">Item Type to check for charges</param>
    private void TryEquipItem(ItemType itemType)
    {
        if (InventoryManager.GetItemCount(itemType) > 0)
        {
            AudioManager.audioManager.PlayUISelect();
            InventoryManager.SetItem(itemType);
        }

    }

    #endregion

    #region Singleton Stuff

    private static UserInterfaceManager _inst = null;

    protected virtual void Awake()
    {
        if(_inst != null)
        {
            DestroyImmediate(this.gameObject);
        }
        _inst = this;
    }

    #endregion

}