﻿using GoogleARCore;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Manages the spawning of plants within the ARCore detected universe.
/// </summary>
public class PlantSpawner : MonoBehaviour
{
    public GameObject plantContainer;
    public List<GameObject> plantPrefabs;
    public float minPlantDist = 1.0f;
    public float minSpawnTime = 10.0f;
    public float maxSpawnTime = 15.0f;

    private List<GameObject> _activePlants = new List<GameObject>();

    private void Start()
    {
        StartCoroutine(SpawnPlant());
    }

    /// <summary>
    /// Spawns plants. This is done by randomly selecting a plane from the
    /// ARCore detected planes, then spawning a random plant on the line to
    /// a randomly select bounding point from the center point.
    /// </summary>
    /// <returns>IEnumerator operation</returns>
    public static IEnumerator SpawnPlant()
    {
        //Wait some time before spawning a plant
        yield return new WaitForSeconds(Random.Range(_inst.minSpawnTime, _inst.maxSpawnTime));
        if(_inst.plantPrefabs.Count > 0)
        {
            List<DetectedPlane> allPlanes = PlaneManager.GetAllPlanes();
            if (allPlanes.Count > 0)
            {
                //Try to spawn a plant ten times before giving up
                for (int i = 0; i < 10; i++)
                {
                    //Choose a random plane
                    DetectedPlane plane = allPlanes[Random.Range(0, allPlanes.Count)];
                    List<Vector3> _boundingPoints = new List<Vector3>();
                    plane.GetBoundaryPolygon(_boundingPoints);
                    if (_boundingPoints.Count > 0)
                    {
                        //Chose a random bounding point
                        Vector3 point = _boundingPoints[Random.Range(0, _boundingPoints.Count)];

                        //Calculate a random point along the line from the center of the
                        //plane to the chosen bounding point upon which to spawn a plant.
                        Vector3 dirToPoint = point - plane.CenterPose.position;
                        float randomDist = Random.Range(0, dirToPoint.magnitude);
                        Vector3 plantPosition = plane.CenterPose.position + (dirToPoint.normalized * randomDist);

                        //If another plant is too close, try again
                        if(_inst._activePlants.Any(gObj => Vector3.Distance(gObj.transform.position, plantPosition) < _inst.minPlantDist))
                        {
                            continue;
                        }

                        //Spawn the newly created plant and begin tracking it. Stop spawning a plant.
                        GameObject newPlant = Instantiate(_inst.plantPrefabs[Random.Range(0, _inst.plantPrefabs.Count)], plantPosition, Quaternion.identity, _inst.plantContainer.transform);
                        AudioManager.audioManager.PlayGroundAppear();
                        _inst._activePlants.Add(newPlant);
                        break;
                    }
                }
            }
        }
        //Repeat the process
        _inst.StartCoroutine(SpawnPlant());
    }

    /// <summary>
    /// Stop tracking a plant that this manager has spawned.
    /// </summary>
    /// <param name="plant">The plant to stop tracking.</param>
    public static void DestroyPlant(GameObject plant)
    {
        _inst._activePlants.Remove(plant);
    }

    /// <summary>
    /// Helpful debugging visualizations.
    /// </summary>
    private void OnDrawGizmos()
    {
        if (Session.Status == SessionStatus.Tracking)
        {
            float sphereRadius = 0.01f;

            List<DetectedPlane> allPlanes = PlaneManager.GetAllPlanes();
            foreach (DetectedPlane plane in allPlanes)
            {
                //Visualize the center of each plane.
                Gizmos.DrawSphere(plane.CenterPose.position, sphereRadius);
                List<Vector3> _boundingPoints = new List<Vector3>();
                plane.GetBoundaryPolygon(_boundingPoints);
                foreach (Vector3 point in _boundingPoints)
                {
                    //Visualize the bounds of each plane.
                    Gizmos.DrawSphere(point, sphereRadius);
                    //Visualize the line upon which plants can spawn.
                    Gizmos.DrawLine(plane.CenterPose.position, point);
                }
            }
        }
    }

    #region Singleton Stuff

    private static PlantSpawner _inst = null;

    private void Awake()
    {
        if(_inst != null)
        {
            DestroyImmediate(this.gameObject);
        }
        _inst = this;
    }

    #endregion
}