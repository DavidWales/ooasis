﻿
/// <summary>
/// The item types within the inventory system.
/// </summary>
public enum ItemType
{
    WATERING_CAN = 0,
    PLANTATION_SEED = 1,
    FOOD_SEED = 2,
    ANIMAL_SEED = 3,
    RANDOM_SEED = 4
}