﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Custom button that will send a notification to the User Interface Manager to swap items.
/// </summary>
public class ItemButton : MonoBehaviour
{
    public ItemType ItemType;

    private Button button;

    private void Awake()
    {
        button = this.GetComponent<Button>();
        button.onClick.AddListener(OnButtonClick);
    }

    /// <summary>
    /// When the button has been clicked, fire a click notification to the User Interface Manager.
    /// </summary>
    private void OnButtonClick()
    {
        UserInterfaceManager.OnItemButtonClick(ItemType);
    }

}
