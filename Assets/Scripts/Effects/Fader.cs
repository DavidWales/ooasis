﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Generic color fade in and fade out behavior
/// </summary>
public abstract class Fader : MonoBehaviour
{
    public float fadeInTime = 2.0f;
    public float fadeOutTime = 2.0f;

    #region Fading Methods

    /// <summary>
    /// Start process of fading in the resource
    /// </summary>
    /// <param name="callback">Method to call after completely faded in</param>
    public void FadeIn(Action callback)
    {
        StartCoroutine(FadeInCoroutine(callback));
    }

    /// <summary>
    /// Slowly fade in the resource over "fadeInTime" seconds
    /// </summary>
    /// <param name="callback">Method to call after completely faded in</param>
    /// <returns></returns>
    private IEnumerator FadeInCoroutine(Action callback)
    {
        Color c;
        while (GetColor().a < 1.0f)
        {
            c = GetColor();
            c.a += Time.unscaledDeltaTime * (1.0f / fadeInTime);
            SetColor(c);
            yield return null;
        }

        c = GetColor();
        c.a = 1.0f;
        SetColor(c);

        callback();
    }

    /// <summary>
    /// Slowly decrease alpha until fully invisible
    /// </summary>
    /// <param name="callback">Method to call after completely faded out</param>
    public void FadeOut(Action callback)
    {
        StartCoroutine(FadeOutCoroutine(callback));
    }

    /// <summary>
    /// Slowly fade out the resource over "fadeOutTime" seconds
    /// </summary>
    /// <param name="callback">Method to call after completely faded out</param>
    /// <returns></returns>
    private IEnumerator FadeOutCoroutine(Action callback)
    {
        Color c;
        while (GetColor().a > 0.0f)
        {
            c = GetColor();
            c.a -= Time.unscaledDeltaTime * (1.0f / fadeOutTime);
            SetColor(c);
            yield return null;
        }

        c = GetColor();
        c.a = 0.0f;
        SetColor(c);

        callback();
    }

    #endregion

    #region Abstraction Methods

    /// <summary>
    /// Used to abstract color property get from functionality
    /// </summary>
    /// <returns>The color property of the resource</returns>
    protected abstract Color GetColor();

    /// <summary>
    /// Used to abstract color property set from functionality
    /// </summary>
    /// <param name="c">The color the resource should be set to</param>
    protected abstract void SetColor(Color c);

    #endregion

}
