﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Helper script to destroy particle system after completed playback
/// </summary>
public class AutoDestroyParticle : MonoBehaviour
{
    private ParticleSystem pSystem;

    private void Awake()
    {
        pSystem = this.GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        //If the particle system is done, destroy it
        if(pSystem.IsAlive() == false)
        {
            Destroy(this.gameObject);
        }
    }
}