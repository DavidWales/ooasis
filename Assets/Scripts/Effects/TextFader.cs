﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Text implementation of fader fuctionality
/// </summary>
[RequireComponent(typeof(Text))]
public class TextFader : Fader
{
    private Text text;

    /// <summary>
    /// Get the text component so we can operate on it's properties
    /// </summary>
    void Awake()
    {
        text = this.GetComponent<Text>();
    }

    /// <summary>
    /// Retrieves the color component of the text to successfully implement the fader class
    /// </summary>
    /// <returns>The color component of the text</returns>
    protected override Color GetColor()
    {
        return text.color;
    }

    /// <summary>
    /// Sets the color of the text. Called by the fader class to fade in and out.
    /// </summary>
    /// <param name="c">The color that the text should be set to</param>
    protected override void SetColor(Color c)
    {
        text.color = c;
    }

}
