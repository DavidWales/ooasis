﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Raw image implementation of fader fuctionality
/// </summary>
[RequireComponent(typeof(RawImage))]
public class RawImageFader : Fader
{
    private RawImage rawImage;

    /// <summary>
    /// Get the raw image component so we can operate on it's properties
    /// </summary>
    void Awake()
    {
        rawImage = this.GetComponent<RawImage>();
    }

    /// <summary>
    /// Retrieves the color component of the raw image to successfully implement the fader class
    /// </summary>
    /// <returns>The color component of the raw image</returns>
    protected override Color GetColor()
    {
        return rawImage.color;
    }

    /// <summary>
    /// Sets the color of the raw image. Called by the fader class to fade in and out.
    /// </summary>
    /// <param name="c">The color that the raw image should be set to</param>
    protected override void SetColor(Color c)
    {
        rawImage.color = c;
    }

}
