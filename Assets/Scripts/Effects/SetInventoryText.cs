﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Sets the text of a "text" component based on the given ItemType's count in
/// the inventory.
/// </summary>
public class SetInventoryText : MonoBehaviour
{
    public ItemType ItemType;
    private Text text;

    private void Awake()
    {
        text = this.GetComponent<Text>();
    }

    private void Update()
    {
        //Set the value of the text to the count of the ItemType in the inventory
        text.text = InventoryManager.GetItemCount(ItemType).ToString();
    }
}