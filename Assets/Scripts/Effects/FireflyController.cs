﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireflyController : MonoBehaviour
{
    Vector3 locationToPlayer;

    GameObject player;

    private void Start()
    {
        this.player = GameObject.Find("ARCore Device");
        this.locationToPlayer = this.transform.localPosition;
    }

    public void ActivateFireFly()
    {
        this.transform.parent = null;
        this.gameObject.SetActive(true);
    }

    public void DeactivateFireFly()
    {
        this.gameObject.SetActive(false);
        this.transform.parent = this.player.transform;
        this.transform.localPosition = this.locationToPlayer;
    }

}
