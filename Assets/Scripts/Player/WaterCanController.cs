﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Controls the water can behavior.
/// </summary>
[RequireComponent(typeof(ParticleSystem))]
public class WaterCanController : MonoBehaviour
{

    #region Properties

    public float waterDistance = 100.0f;
    public float minEmitAngle = 45.0f;
    public float maxEmitAngle = 0.0f;
    public GameObject wateringCan;
    public GameObject smallWaterDropletSpawner;

    private ParticleSystem pSystem;
    private ParticleSystem.EmissionModule particleEmissionModule;
    private float maxParticles = 0.0f;
    private float waterDensity = 0.0f;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        pSystem = this.GetComponent<ParticleSystem>();
        particleEmissionModule = pSystem.emission;
        maxParticles = particleEmissionModule.rateOverTimeMultiplier;
        SetWaterDensity();
    }

    private void Update()
    {
        SetWaterDensity();
        if(waterDensity > 0.0f)
        {
            //Find all of the plants in the front of the user and water them
            RaycastHit[] objsHit = Physics.SphereCastAll(this.transform.position, 0.5f,
                Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, waterDistance)));
            foreach(RaycastHit hit in objsHit)
            {
                BasePlant plant = hit.collider.GetComponent<BasePlant>();
                if (plant == null) continue;
                plant.Water(waterDensity);
            }

            //Handle the state of the splash effect
            HandleSplashEffect();
        }
        else
        {
            //Turn the splash effect off if no water is coming out of the water can.
            smallWaterDropletSpawner.SetActive(false);
        }
        CheckWaterSound();
    }

    /// <summary>
    /// Helpful visualization tools to show where we are watering.
    /// </summary>
    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(this.transform.position, (Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, waterDistance)) - this.transform.position).normalized);
        Gizmos.DrawSphere(Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, waterDistance)), 0.5f);
    }

    #endregion

    #region Helper Methods

    /// <summary>
    /// Controls the location and active state of the splash particle effect based on the
    /// water from the water can.
    /// </summary>
    private void HandleSplashEffect()
    {
        //Find the oldest living particle, and put the splash particle system at it's location.
        ParticleSystem.Particle[] allParticles = new ParticleSystem.Particle[pSystem.particleCount];
        this.GetComponent<ParticleSystem>().GetParticles(allParticles);
        ParticleSystem.Particle? lastParticle = GetOldestParticle(allParticles);
        if (lastParticle.HasValue && (lastParticle.Value.remainingLifetime / pSystem.main.duration) < 0.05)
        {
            //Set the splash effect to point upwards at the oldest living particle's location.
            smallWaterDropletSpawner.SetActive(true);
            smallWaterDropletSpawner.transform.rotation = Quaternion.LookRotation(Vector3.up, Vector3.forward);
            smallWaterDropletSpawner.transform.position = lastParticle.Value.position;
        }
        else
        {
            smallWaterDropletSpawner.SetActive(false);
        }
    }

    /// <summary>
    /// Determines whether or not to play the watering noise
    /// </summary>
    private void CheckWaterSound()
    {
        if (waterDensity > 0.0f)
        {
            AudioManager.audioManager.PlayWateringNoise();
        }
        else
        {
            AudioManager.audioManager.StopWateringNoise();
        }
    }

    /// <summary>
    /// Determines the density of the water coming out of the water can based on the
    /// angle that the water can is tilted.
    /// </summary>
    private void SetWaterDensity()
    {
        float rotDiff = Mathf.Abs(maxEmitAngle - wateringCan.transform.eulerAngles.x);

        waterDensity = 0.0f;
        if(rotDiff < minEmitAngle)
        {
            waterDensity = (1.0f - (rotDiff / minEmitAngle));
        }
        particleEmissionModule.rateOverTimeMultiplier = waterDensity * maxParticles;
    }

    /// <summary>
    /// Finds the oldest living particle so that we can set where the splash particle
    /// effect should be located.
    /// </summary>
    /// <param name="particles">All of the particles for a given particle system.</param>
    /// <returns>The oldest living particle in the given array of particles.</returns>
    private ParticleSystem.Particle? GetOldestParticle(ParticleSystem.Particle[] particles)
    {
        ParticleSystem.Particle? result = null;
        foreach(ParticleSystem.Particle p in particles)
        {
            if(result.HasValue == false || p.remainingLifetime < result.Value.remainingLifetime)
            {
                result = p;
            }
        }
        return result;
    }

    #endregion

}