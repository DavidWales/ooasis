﻿using GoogleARCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controls user input for placing seeds from the inventory. It is also in charge
/// of updating the inventory count and reporting an error if an invalid location
/// was selected for seed placement.
/// </summary>
public class SeedController : MonoBehaviour
{
    public ItemType ItemType;
    public List<GameObject> PlantPrefabs;
    public Transform PlantContainer;
    public TextFader cantPlantText;

    /// <summary>
    /// Every frame, determine if input was detected, and try to place a seed.
    /// </summary>
    protected virtual void Update()
    {
        TryPlaceSeed(GetInputPosition());
    }

    /// <summary>
    /// Attempts to get input from the user and determine where the input was
    /// detected in camera space. Currently set up to detect input from both
    /// touch actions and mouse clicks for debugging purposes.
    /// </summary>
    /// <returns>The location, in camera space, where input was detected</returns>
    protected virtual Vector2? GetInputPosition()
    {
        Vector2? clickPos = null;
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                clickPos = touch.position;
            }
        }
        else if (Input.GetMouseButtonDown(0))
        {
            clickPos = Input.mousePosition;
        }
        return clickPos;
    }

    /// <summary>
    /// Spawns a random plant of the given category. If a seed cannot be
    /// placed, or no location was provided, do nothing.
    /// </summary>
    /// <param name="clickPos">Location, in camera space, to place the plant</param>
    protected virtual void TryPlaceSeed(Vector2? clickPos)
    {
        //Only try to place seed if screen is clicked
        if (clickPos.HasValue)
        {
            TrackableHit hit;
            TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon |
                TrackableHitFlags.FeaturePointWithSurfaceNormal;

            //If any plane is hit
            if (Frame.Raycast(clickPos.Value.x, clickPos.Value.y, raycastFilter, out hit))
            {
                //Spawn random plant at point on plane
                GameObject randomPlant = PlantPrefabs[Random.Range(0, PlantPrefabs.Count)];
                Instantiate(randomPlant,
                    hit.Pose.position,
                    Quaternion.LookRotation(Vector3.forward),
                    PlantContainer);

                //Hand inventory feedback
                InventoryManager.AddItemCount(ItemType, -1);
                if (InventoryManager.GetItemCount(ItemType) <= 0)
                {
                    InventoryManager.SetItem(ItemType.WATERING_CAN);
                }
            }
            else
            {
                //Display warning that location is invalid
                cantPlantText.FadeIn(() =>
                {
                    cantPlantText.FadeOut(() => { });
                });
            }
        }
    }
}