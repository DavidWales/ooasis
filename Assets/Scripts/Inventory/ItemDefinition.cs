﻿using System;
using UnityEngine;

/// <summary>
/// This class defines the properties of an inventory item.
/// </summary>
[Serializable]
public class ItemDefinition
{
    public ItemType type;
    public GameObject obj;
}