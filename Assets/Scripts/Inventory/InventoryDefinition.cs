﻿using System;

/// <summary>
/// This class defines what properties make up an inventory slot.
/// </summary>
[Serializable]
public class InventoryDefinition
{
    public ItemType type;
    public int count;
}