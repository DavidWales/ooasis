﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controls the evolution of a plant
/// </summary>
public class PlantEvolver : MonoBehaviour
{

    #region Properties

    public BasePlant currentPlant;
    public List<GameObject> evolutionPrefabs;
    public int currentPlantIndex = 0;
    public GameObject evolutionSmokeEffect;
    public ItemType ItemType;
    public int RewardCount = 2;

    private bool rewarded = false;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        if(this.transform.childCount <= 0)
        {
            //If there is no plant currently active, instantiate the first plant as the plant at the specified index
            GameObject newObj = Instantiate(evolutionPrefabs[currentPlantIndex], this.transform.position, Quaternion.identity, transform);
            BasePlant newPlant = newObj.GetComponent<BasePlant>();
            currentPlant = newPlant;
        }
    }

    protected virtual void Update()
    {
        if(currentPlant.IsPlantFlourishing())
        {
            //If the plant is flourishing, evolve it.
            AudioManager.audioManager.PlayWateringComplete();
            ReplacePlant(1);
        }
        else if(currentPlant.IsPlantDying())
        {
            //If the plant is dying, devolve it.
            ReplacePlant(-1);

            //Once the plant is devolved, set the gathered water to slightly below required.
            currentPlant.gatheredWater = currentPlant.waterRequired;
            currentPlant.Water(-1.0f);
        }
    }

    #endregion

    #region Evolution Methods

    /// <summary>
    /// Replaces the plant with the next or prior evolution, depending on whether it is flourishing or dying.
    /// </summary>
    /// <param name="offset">The direction in the evolution to change to (+ is evolution, - is devolution).</param>
    protected virtual void ReplacePlant(int offset)
    {
        int newPlantIndex = currentPlantIndex + offset;
        if (newPlantIndex >= 0 && newPlantIndex < evolutionPrefabs.Count)
        {
            //Create smoke effect and evolve plant. Set current state in evolution.
            CreateSmoke(currentPlant.gameObject, evolutionPrefabs[newPlantIndex]);
            EvolvePlant(evolutionPrefabs[newPlantIndex]);
            currentPlantIndex = newPlantIndex;

            //If we have reached the final stage of evolution, reward the user (if not already rewarded)
            if(currentPlantIndex >= evolutionPrefabs.Count - 1 && rewarded == false)
            {
                InventoryManager.AddItemCount(ItemType, RewardCount);
                rewarded = true;
            }
        }
        else if(newPlantIndex < 0)
        {
            //Destroy the evolution if the lowest stage in the evolution withers
            PlantSpawner.DestroyPlant(this.gameObject);
            Destroy(this.gameObject);
        }
    }

    /// <summary>
    /// Veils the evolution of a plant by creating a smoke effect.
    /// </summary>
    /// <param name="oldPlant">The plant it is evolving from.</param>
    /// <param name="newPlant">The plant it is evolving to.</param>
    protected virtual void CreateSmoke(GameObject oldPlant, GameObject newPlant)
    {
        Vector3 smokeSize = Vector3.Max(oldPlant.GetComponent<Collider>().bounds.size,
            newPlant.GetComponent<Collider>().bounds.size) + (Vector3.one * 0.1f);
        GameObject smokeEffect = Instantiate(evolutionSmokeEffect, this.transform.position + (Vector3.up * (smokeSize.y / 2.0f)), Quaternion.identity);
        ParticleSystem.ShapeModule smokeShape = smokeEffect.GetComponent<ParticleSystem>().shape;
        smokeShape.scale = smokeSize;
    }

    /// <summary>
    /// Evolves the plant by instantiating the new plant and destroying the old plant.
    /// </summary>
    /// <param name="newPlant"></param>
    /// <returns></returns>
    protected virtual BasePlant EvolvePlant(GameObject newPlant)
    {
        GameObject newObj = Instantiate(newPlant, this.transform.position, Quaternion.identity, transform);
        BasePlant plant = newObj.GetComponent<BasePlant>();
        Debug.Assert(newPlant != null, "There is no BasePlant script on the " + newObj.name + " evolution.");
        Destroy(currentPlant.gameObject);
        currentPlant = plant;
        return plant;
    }

    #endregion

}