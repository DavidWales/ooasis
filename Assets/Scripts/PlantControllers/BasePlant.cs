﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base Plant implementation with watering and withering functionality.
/// </summary>
public class BasePlant : MonoBehaviour
{
    public Vector3 minScale = Vector3.one;
    public Vector3 maxScale = Vector3.one;
    public float waterRequired = 15.0f;
    public float gatheredWater = 5.0f;
    public float witherTime = 10.0f;
    public float witherAmount = 1.0f;
    public bool canWither = false;
    public bool isSprout = false;

    protected virtual void Awake()
    {
        SetObjectScale();
        StartCoroutine(Wither());
    }

    private void Start()
    {
        if (isSprout)
        {
            AudioManager.audioManager.PlaySproutAppear();
        }
    }

    /// <summary>
    /// Adds water to the plant.
    /// </summary>
    /// <param name="waterDensity">Amount of water to add.</param>
    public virtual void Water(float waterDensity)
    {
        gatheredWater += (waterDensity * Time.deltaTime);
        if (gatheredWater > waterRequired) gatheredWater = waterRequired;
        SetObjectScale();
        SetObjectRotation();
    }

    /// <summary>
    /// Sets the scale of the object based on the relative water gathered.
    /// </summary>
    private void SetObjectScale()
    {
        this.transform.localScale = minScale + ((maxScale - minScale) * (gatheredWater / waterRequired));
    }

    /// <summary>
    /// Sets the rotation of the object based on the relative water gathered.
    /// </summary>
    private void SetObjectRotation()
    {
        this.transform.localRotation = Quaternion.Euler(this.transform.localRotation.x, (gatheredWater / waterRequired), this.transform.localRotation.z);
    }

    /// <summary>
    /// Withers the plant over time if not watered since last check.
    /// </summary>
    /// <returns>IEnumerator Operations</returns>
    IEnumerator Wither()
    {
        if (canWither)
        {
            float currentWater = gatheredWater;
            yield return new WaitForSeconds(witherTime);
            if (Mathf.Abs(gatheredWater - currentWater) < 0.1f)
            {
                gatheredWater -= witherAmount;
            }
            StartCoroutine(Wither());
        }
    }

    /// <summary>
    /// Determines whether the plant is flourishing. This happens when there is
    /// more gathered water than required.
    /// </summary>
    /// <returns>True/False depending on whether plant is flourishing.</returns>
    public virtual bool IsPlantFlourishing()
    {
        return gatheredWater >= waterRequired;
    }

    /// <summary>
    /// Determines whether the plant is dying. This happens when the plant has
    /// no water left.
    /// </summary>
    /// <returns>True/False depending on whether the plant is dying.</returns>
    public virtual bool IsPlantDying()
    {
        return gatheredWater <= 0;
    }
}